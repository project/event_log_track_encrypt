<?php

/**
 * @file
 * Hook implementations for the Event Log Track Encrypt module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Drupal\event_log_track_encrypt\Tags\EncryptTags;

/**
 * Implements hook_event_log_track_alter().
 */
function event_log_track_encrypt_event_log_track_alter(&$log) {
  $event_types_to_encrypt = \Drupal::configFactory()
    ->get('event_log_track.encrypt.settings')
    ->get('event_types_to_encrypt');
  $event_type = $log['type'];
  if (!empty($event_types_to_encrypt["$event_type"])) {
    $encryption_profile = EncryptionProfile::load('event_log_track_encryption');
    $encrypted_desc = \Drupal::service('encryption')
      ->encrypt($log['description'], $encryption_profile);
    $log['description'] = EncryptTags::TAG_START . $encrypted_desc . EncryptTags::TAG_END;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for events_track_form.
 *
 * Add encryption configuration to events_track_form.
 */
function event_log_track_encrypt_form_events_track_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::configFactory()
    ->getEditable('event_log_track.encrypt.settings');
  $enable_encrypt = $config->get('enable_encrypt');

  $form['encrypt'] = [
    '#type' => 'fieldset',
    '#title' => t('Encryption'),
    'enable_encrypt' => [
      '#type' => 'checkbox',
      '#title' => t('Enable encryption'),
      '#default_value' => $enable_encrypt,
      '#description' => t('Enable encryption logs'),
    ],
    'event_types' => [
      '#type' => 'fieldset',
      '#title' => t('Event types to encrypt'),
      '#description' => t('Descriptions of events selected here will be encrypted.'),
      '#states' => [
        'visible' => [
          ':input[name="enable_encrypt"]' => ['checked' => TRUE],
        ],
      ],
    ],
  ];

  $event_types = _event_log_track_encrypt_event_types_support_encryption();
  $event_types_to_encrypt = $config->get('event_types_to_encrypt');
  foreach ($event_types as $event_type) {
    $form['encrypt']['event_types']['encrypt_event_type_' . $event_type] = [
      '#type' => 'checkbox',
      '#title' => $event_type,
      '#default_value' => $event_types_to_encrypt["$event_type"],
    ];
  }

  $form['#submit'][] = '_event_log_track_encrypt_form';
}

/**
 * Save encrypt config.
 */
function _event_log_track_encrypt_form(array &$form, FormStateInterface $form_state) {
  $event_types = _event_log_track_encrypt_event_types_support_encryption();
  $event_types_to_encrypt = [];
  foreach ($event_types as $event_type) {
    $event_types_to_encrypt[$event_type] = $form_state->getValue('encrypt_event_type_' . $event_type);
  }
  $config = \Drupal::configFactory()
    ->getEditable('event_log_track.encrypt.settings');
  $config->set('enable_encrypt', $form_state->getValue('enable_encrypt'))
    ->set('event_types_to_encrypt', $event_types_to_encrypt)
    ->save();
}

/**
 * List of event types supporting encryption.
 */
function _event_log_track_encrypt_event_types_support_encryption() {
  return [
    'authentication',
    'authorization',
    'cache_clear',
    'comment',
    'config',
    'file',
    'group',
    'media',
    'menu',
    'node',
    'taxonomy',
    'user',
    'workflows',
  ];
}
