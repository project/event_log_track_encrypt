<?php

namespace Drupal\event_log_track_encrypt\Tags;

/**
 * Defines constants to tag encrypted strings.
 */
final class EncryptTags {

  // Tag indicating start of encrypted string.
  const TAG_START = '@@crypt@@';

  // Tag indicating end of encrypted string.
  const TAG_END = '@@endcrypt@@';

}
