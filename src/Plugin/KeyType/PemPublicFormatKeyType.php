<?php

namespace Drupal\event_log_track_encrypt\Plugin\KeyType;

use Drupal\key\Plugin\KeyPluginFormInterface;

/**
 * Defines a generic key type for encryption.
 *
 * @KeyType(
 *   id = "public_pem",
 *   label = @Translation("Public key (PEM)"),
 *   description = @Translation("A public key type to using PEM format."),
 *   group = "encryption",
 *   key_value = {
 *     "plugin" = "textarea_field"
 *   }
 * )
 */
class PemPublicFormatKeyType extends PemFormatKeyTypeBase implements KeyPluginFormInterface {

  /**
   * {@inheritdoc}
   */
  protected function getKeyDetails($key_value) {
    $key = openssl_get_publickey($key_value);
    return $key ? openssl_pkey_get_details($key) : FALSE;
  }

}
