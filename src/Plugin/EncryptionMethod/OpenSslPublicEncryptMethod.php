<?php

namespace Drupal\event_log_track_encrypt\Plugin\EncryptionMethod;

use Drupal\encrypt\Plugin\EncryptionMethod\EncryptionMethodBase;

/**
 * Provide a public key encryption method.
 *
 * This encryption method use openssl_public_encrypt() to encrypt data with
 * public key.
 *
 * @see https://www.php.net/manual/en/function.openssl-public-encrypt.php
 * @see https://www.php.net/manual/en/function.openssl-private-decrypt.php
 *
 * @EncryptionMethod(
 *   id = "openssl_public_encrypt",
 *   title = @Translation("OpenSSL Public Encrypt"),
 *   description = @Translation("Encrypts data with public key (openssl_public_encrypt() method)"),
 *   key_type = {"public_pem"}
 * )
 */
class OpenSslPublicEncryptMethod extends EncryptionMethodBase {

  const ENVELOPE_SEPARATOR = ',';

  const CIPHER_ALGO = 'AES256';

  /**
   * Encrypt text.
   *
   * @param string $text
   *   The text to be encrypted.
   * @param string $key
   *   The key to encrypt the text with.
   *
   * @return string
   *   The base64 encrypted message + random key, separated by
   *   ENVELOPE_SEPARATOR.
   */
  public function encrypt($text, $key) {
    $iv = openssl_random_pseudo_bytes(32);
    if (openssl_seal($text, $sealed, $ekeys, [$key], static::CIPHER_ALGO, $iv) !== FALSE) {
      return base64_encode($sealed)
        . static::ENVELOPE_SEPARATOR
        . base64_encode($ekeys[0])
        . static::ENVELOPE_SEPARATOR
        . base64_encode($iv);
    }
    return 'encrypt error';
  }

  /**
   * {@inheritdoc}
   */
  public function decrypt($text, $key) {
    // You can't decrypt data with a Public key.
    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public function checkDependencies($text = NULL, $key = NULL) {
    $errors = [];

    if (!extension_loaded('openssl')) {
      $errors[] = $this->t('OpenSSL extensions is required for this Encryption Method.');
    }

    return $errors;
  }

}
