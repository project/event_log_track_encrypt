<?php

namespace Drupal\event_log_track_encrypt\Commands;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\event_log_track_encrypt\Plugin\EncryptionMethod\OpenSslPublicEncryptMethod;
use Drupal\event_log_track_encrypt\Tags\EncryptTags;
use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\event_log_track_decrypt_commands\Commands
 */
class EventLogTrackDecryptCommands extends DrushCommands {

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The module_handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Output file handler.
   *
   * @var null
   */
  private $outputFileHandler = NULL;

  /**
   * Output file name.
   *
   * @var string
   */
  private $outputFileName = '';

  /**
   * Constructs a new EventLogTrackDecryptCommands object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module_handler service.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $moduleHandler) {
    $this->connection = $connection;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('database'),
      $container->get('module_handler')
    );
  }

  /**
   * Decrypt data with the given private key.
   *
   * @param string $encryptedData
   *   Encrypted data.
   * @param \OpenSSLAsymmetricKey $privateKey
   *   Private key object.
   *
   * @return string
   *   Return decrypted data.
   *
   * @throws \Exception
   */
  private function decrypt(string $encryptedData, \OpenSSLAsymmetricKey $privateKey) {
    // Split envelope.
    $envelope = explode(OpenSslPublicEncryptMethod::ENVELOPE_SEPARATOR, $encryptedData);

    // Check envelope integrity.
    if (count($envelope) !== 3) {
      return $encryptedData;
    }

    // Base64 decode each pieces.
    $data = base64_decode($envelope[0]);
    $ekey = base64_decode($envelope[1]);
    $iv = base64_decode($envelope[2]);

    // Decrypt data.
    if (!openssl_open($data, $decryptedData, $ekey, $privateKey, OpenSslPublicEncryptMethod::CIPHER_ALGO, $iv)) {
      throw new \Exception(dt('Decryption failed !'));
    }
    return $decryptedData;
  }

  /**
   * Decrypt and replace the string's encrypted section.
   *
   * @param string $data
   *   Data containing encrypted string.
   * @param \OpenSSLAsymmetricKey $privateKey
   *   Private key object.
   *
   * @return array|string|string[]|null
   *   Data containing decrypted string.
   *
   * @throws \Exception
   */
  private function findAndDecrypt(string $data, \OpenSSLAsymmetricKey $privateKey) {
    return preg_replace_callback(
      '/' . EncryptTags::TAG_START . '(.*)' . EncryptTags::TAG_END . '/',
      function ($matches) use ($privateKey) {
        $decrypt = $this->decrypt($matches[1], $privateKey);
        $decrypt = str_replace(["\n", "\r"], ['#012', '#015'], $decrypt);
        return $decrypt;
      },
      $data
    );
  }

  /**
   * Get the private key object from the private key file.
   *
   * @param string $privateKeyFile
   *   Private key file.
   *
   * @return \OpenSSLAsymmetricKey
   *   Private key object.
   *
   * @throws \Exception
   */
  private function getPrivateKey(string $privateKeyFile) {
    if (empty($privateKeyFile)) {
      throw new \Exception(dt("Private key file not provided. Option --private-key-file is required !"));
    }
    if (!file_exists($privateKeyFile)) {
      throw new \Exception(dt("Private key file not found !", ['@pkey' => $privateKeyFile]));
    }

    $passPhrase = $this->io()->askHidden('Passphrase (to unlock private key):');

    $privateKey = openssl_pkey_get_private('file://' . $privateKeyFile, $passPhrase);
    if (!$privateKey) {
      throw new \Exception(dt("Invalid private key or passphrase !"));
    }

    return $privateKey;
  }

  /**
   * Open output file according to --output-file option.
   *
   * @param array $options
   *   All command options.
   *
   * @throws \Exception
   */
  private function outputFileOpen(array $options) {
    $outputFile = $options['output-file'] ?? NULL;
    if (!is_null($outputFile)) {
      if (file_exists($outputFile)) {
        $this->io()
          ->warning('File ' . $outputFile . ' already exists. If you choose to overwrite it, all the data it contains will be lost !');
        $confirm = $this->io()
          ->confirm('Do you want to overwrite it ?', FALSE);
        if (!$confirm) {
          throw new \Exception(dt("Action cancelled ! File @file has not been modified.", ['@file' => $outputFile]));
        }
      }
      $this->outputFileHandler = fopen($outputFile, "w");
      if (!$this->outputFileHandler = fopen($outputFile, "w")) {
        throw new \Exception(dt("Unable to write/create output file @file !", ['@file' => $outputFile]));
      }
      $this->outputFileName = $outputFile;
    }
  }

  /**
   * Close output file if necessary.
   */
  private function outputFileClose() {
    if (!is_null($this->outputFileHandler)) {
      $this->io()->success('Data decrypted successfully in ' . $this->outputFileName . ' !');
      fclose($this->outputFileHandler);
    }
  }

  /**
   * Drush command to decrypt a string.
   *
   * @param string $encryptedString
   *   Encrypted string.
   * @param array $options
   *   All command options.
   *
   * @command elt:decrypt-string
   * @option private-key-file Path to the private key file.
   * @aliases eltds
   * @usage eltds
   *   --private-key-file=/var/keys/event_log_track_private_key.pem
   *   string_to_decrypt
   */
  public function decryptString(
    string $encryptedString,
    array $options = [
      'private-key-file' => '',
    ],
  ) {
    // Get private key.
    $privateKey = $this->getPrivateKey($options['private-key-file']);

    // Decrypt string.
    return $this->decrypt($encryptedString, $privateKey);
  }

  /**
   * Drush command to decrypt database event logs.
   *
   * @param array $options
   *   All command options.
   *
   * @command elt:decrypt-db
   * @option private-key-file Path to the private key file.
   * @option output-file Path to the output csv file.
   * @option table-suffix If set, the query will be executed on the
   * event_log_track_[suffix] table. Can be useful for split very large
   * log tables.
   * @option limit Limit the query result to the first X lines.
   * @option condition Defines a query condition.
   * A condition must be in the format: “[field] [operator] [value]”.
   * [field]: The name of the field in the event_log_track table.
   * [operator] : must be 'LIKE' for text fields and '=', '!=', '<', '>',
   * '<=' or '>=' for numeric fields
   * [value]: must be a number for numeric fields and a string for text fields
   * (the sql wildcard % is accepted - do not delimit the string).
   * You can define several conditions, which will be combined with the
   * AND operator.
   * Example: --condition=“type LIKE co%” --condition=“uid > 1”
   * @aliases eltddb
   * @usage eltddb
   *   --private-key-file=/home/me/keys/event_log_track_private_key.pem
   *   --output-file=/home/me/decrypted_extract.csv
   *   --condition=“type LIKE co%”
   *   --condition=“uid > 1”
   *   --condition="ip LIKE 172.10.0.1"
   *   --limit=1000
   */
  public function dbDecrypt(
    array $options = [
      'private-key-file' => '',
      'output-file' => NULL,
      'table-suffix' => '',
      'limit' => '',
      'condition' => [],
    ],
  ) {
    // Check table-suffix option and get table name.
    $table = 'event_log_track';
    if (!empty($options['table-suffix'])) {
      if (!preg_match("/^[0-9a-zA-Z]+$/", $options['table-suffix'])) {
        throw new \Exception(dt("Invalid table suffix !"));
      }
      $table .= '_' . $options['table-suffix'];
    }

    // Check if table exists.
    if (!$this->connection->schema()->tableExists($table)) {
      throw new \Exception(dt("Table @table does not exist !", ['@table' => $table]));
    }

    // Query instantiation.
    $query = $this->connection->select($table, 'elt')->fields('elt');

    // Check limit option.
    if (!empty($options['limit'])) {
      if (!preg_match("/^[0-9]+$/", $options['limit'])) {
        throw new \Exception(dt("Invalid limit !"));
      }
      $query->range(0, $options['limit']);
    }

    // Check condition option(s).
    if (!empty($options['condition'])) {
      $this->moduleHandler->loadInclude('event_log_track', 'install');
      $schema = event_log_track_schema();
      $elt_fields = $schema['event_log_track']['fields'];

      $pattern = '/^(' . implode('|', array_keys($elt_fields)) . ') ([LIKE<>=!]+) (.+)$/';
      foreach ($options['condition'] as $condition) {
        if (!preg_match($pattern, $condition, $matches)) {
          throw new \Exception(dt("Invalid condition !"));
        }
        $field = $matches[1];
        $operator = strtoupper($matches[2]);
        $value = $matches[3];
        if (
          !in_array($operator, ['=', '!=', '<', '>', '<=', '>=', 'LIKE'])
          || (in_array($elt_fields[$field]['type'], ['serial', 'int']) && $operator === 'LIKE')
          || (in_array($elt_fields[$field]['type'], ['varchar', 'text']) && $operator !== 'LIKE')
        ) {
          throw new \Exception(dt("Invalid operator in condition ! "));
        }
        $query->condition('elt.' . $field, $value, $operator);
      }
    }

    // Get private key.
    $privateKey = $this->getPrivateKey($options['private-key-file']);

    // Open output file if necessary.
    $this->outputFileOpen($options);

    // Read and decrypt event_log_track table.
    $logs = $query->execute()->fetchAll();
    foreach ($logs as $log) {
      // Decrypt data.
      $log->description = $this->findAndDecrypt($log->description, $privateKey);

      // Manage output.
      if (is_null($this->outputFileHandler)) {
        $line = implode(' | ', (array) $log) . "\n";
        echo $line;
      }
      else {
        fputcsv($this->outputFileHandler, (array) $log);
      }
    }

    // Close output file if necessary.
    $this->outputFileClose();
  }

  /**
   * Drush command to decrypt log file.
   *
   * @param string $logFile
   *   Absolute path to log file.
   * @param array $options
   *   All command options.
   *
   * @command elt:decrypt-file
   * @option private-key-file Path to the private key file.
   * @option output-file Path to the output file.
   * @aliases eltdf
   * @usage eltdf
   *   --private-key-file=/home/me/keys/event_log_track_private_key.pem
   *   --output-file=/home/me/decrypted_file.log
   *   /home/me/encrypted_file.log
   */
  public function fileDecrypt(
    string $logFile,
    array $options = [
      'private-key-file' => '',
      'output-file' => NULL,
    ],
  ) {
    // Check log file.
    if (!file_exists($logFile)) {
      throw new \Exception(dt("Log file not found !", ['@file' => $logFile]));
    }
    if (!is_readable($logFile)) {
      throw new \Exception(dt("Log file is not readable !", ['@file' => $logFile]));
    }

    // Get private key.
    $privateKey = $this->getPrivateKey($options['private-key-file']);

    // Open output file if necessary.
    $this->outputFileOpen($options);

    // Read and decrypt encrypted log file.
    $logHandle = fopen($logFile, "r");
    if (!$logHandle) {
      throw new \Exception(dt("Unable to open file @file !", ['@file' => $logFile]));
    }
    while (($line = fgets($logHandle)) !== FALSE) {
      // Decrypt data.
      $line = $this->findAndDecrypt($line, $privateKey);

      // Manage output.
      if (is_null($this->outputFileHandler)) {
        echo $line;
      }
      else {
        fwrite($this->outputFileHandler, $line);
      }
    }
    fclose($logHandle);

    // Close output file if necessary.
    $this->outputFileClose();
  }

}
